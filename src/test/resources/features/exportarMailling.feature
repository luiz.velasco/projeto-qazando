# language: pt

@mailling
Funcionalidade: Exportar Mailling no BPcop
  Como usuario do sistema
  Desejo exportar o mailling
  Para verificar que o download esta sendo feito com suceso

  Cenario: Realizar download de mailling no BPcop
    Dado que estou logado no bpcob
    Quando acesso a tela de contratos
    E aplico o filtro para exportar o mailling
    E dou clique em download
    Entao e feito o download do arquivo de mailling