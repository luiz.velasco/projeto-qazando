# language: pt

@segundoCampoAbas
Funcionalidade: Navegar pelo segundo campo de abas no BPcop
  Como usuario do sistema
  Desejo navegar pelo segundo campo de abas
  Para visualizar todos os campos contidos nelas

  Cenario: Acesso as abas de acionamento no BPcop
    Dado faço login no crm
    Quando abro um contrato
    E clico nas abas cobranca juridico localizador contrarias e boleto
    Entao posso visualizar que é possivel ver os campos em cada aba