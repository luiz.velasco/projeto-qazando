# language: pt

@simularacordo
Funcionalidade: Simulação de acordo no BPcop
  Como usuario do sistema
  Desejo simular um acordo
  Para visualizar a quantidade de parcelas

  Cenario: Simular um acordo no contrato no BPcop
    Dado que acesso um contrato
    Quando escolho a forma de pagamento
    E clico em calcular
    Entao posso visualizar a quantidade de parcelas selecionada