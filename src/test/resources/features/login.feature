# Passo 1 - Ler o Gherkin

# language: pt
@login
Funcionalidade: Login no BPcop
  Como usuario do sistema
  Desejo fazer login
  Para fazer uma consulta no CRM

  Cenario: Login com sucesso no BPcop
    Dado que acesso a aplicacao
    E acesso a tela de login
    Quando preencho login e senha
    E clico em Login
    Entao vejo mensagem de login com sucesso