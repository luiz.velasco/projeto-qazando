# language: pt

@acionamento
Funcionalidade: Gravar acionamento no BPcop
  Como usuario do sistema
  Desejo acessar um contrato
  Para gravar um acionamento

  Cenario: Gravar acionamento com sucesso no BPcop
    Dado que faco login no crm
    Quando acesso um contrato
    E preencho os campos de acionamento
    E incluo o acionamento
    Então o crm apresenta o acionamento gravado