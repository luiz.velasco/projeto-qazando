# language: pt

@contratos
Funcionalidade: Acessar contratos no BPcop
  Como usuario do sistema
  Desejo realizar um filtro de contratos
  Para verificar o retorno dos contratos

  Cenario: Carregamento de contratos no BPcop
    Dado que estou logado no crm
    Quando acesso a tela de relatorios
    E preencho os campos de filtro
    E realizo a pesquisa
    Então o crm apresenta os contratos