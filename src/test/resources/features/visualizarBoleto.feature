# language: pt

@boleto
Funcionalidade: Visualizar boleto no BPcop
  Como usuario do sistema
  Desejo acessar a tela de boleto
  Para visualizar um boleto

  Cenario: Acesso ao boleto no BPcop
    Dado realizo login no crm
    Quando consulto um contrato
    E clico na aba de boletos
    E clico em visualizar boleto
    Entao posso visualizar um boleto em pdf