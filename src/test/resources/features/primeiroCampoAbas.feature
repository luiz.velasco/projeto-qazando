# language: pt

@primeiroCampoAbas
Funcionalidade: Navegar pelo primeiro campo de abas no BPcop
  Como usuario do sistema
  Desejo navegar pelo primeiro campo de abas
  Para visualizar todos os campos contidos nelas

  Cenario: Acesso as abas cpom dados do cliente no BPcop
    Dado acesso o crm
    Quando faco a consulta de um contrato
    E clico nas abas telefones enderecos avalistas informacoes acompanhamento historico
    Entao posso visualizar todos os campos contidos nelas