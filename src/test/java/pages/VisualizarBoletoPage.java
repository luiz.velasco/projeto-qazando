package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import runner.RunCucumber;

public class VisualizarBoletoPage extends RunCucumber {

    public void consultaContrato() {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=78059572");
    }

    public void cliqueBoleto() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,950)", "");
        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsAcionamento\']/div/ul/li[5]/a/span/span/span")).click();
    }

    public void cliqueVisualizarBoleto() {
        driver.findElement(By.xpath("//*[@id=\'PH_gvBolCli\']/tbody/tr[2]/td[7]/a")).click();
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VisualizarBoleto.aspx?cdBoleto=53327988");
        driver.findElement(By.id("PH_hplVerBoleto")).click();
    }

    public void validacaoBoleto() {
        getDriver().get("https://boletos.bellinatiperez.com.br/ViewBoleto.aspx?cedente=62075&boleto=26222530015952099&cedentecc=&sacadoChave=B5qDoGLzH11RXqiqU1aj3Q%3d%3d&bel=123987");
        driver.findElement(By.id("btnsms")).isDisplayed();
        driver.quit();
    }
}
