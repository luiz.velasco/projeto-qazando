package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import runner.RunCucumber;

public class GravarAcionamentosPage extends RunCucumber {

    public void contratoParaAcionamento() throws InterruptedException {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=85515513");
        Thread.sleep(2000);
    }

    public void camposAcionamentoPreenchidos() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,650)", "");
        Thread.sleep(2000);

        driver.findElement(By.id("PH_ddlCategoriaEventoAcionamentoCobranca")).click();
        driver.findElement(By.xpath("//*[@id=\'PH_ddlCategoriaEventoAcionamentoCobranca\']/option[9]")).click(); // categoria receptivo
        driver.findElement(By.id("PH_ddlEventoAcionamentoCobranca")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\'PH_ddlEventoAcionamentoCobranca\']/option[8]")).click(); // Evento
        Thread.sleep(2000);
        driver.findElement(By.id("PH_ddlFoneContato")).click();
        driver.findElement(By.xpath("//*[@id=\'PH_ddlFoneContato\']/option[2]")).click(); // contato
        Thread.sleep(1000);
        driver.findElement(By.id("PH_txtInteracao")).sendKeys("Teste automatizado QA");
        Thread.sleep(1000);
        driver.findElement(By.id("PH_btnIncluirAcionamentoCobranca")).click();
    }

    public void incluirAcionamento() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.id("PH_btnIncluirAcionamentoCobranca")).click();

    }

    public void mensagemAcionamento() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.id("PH_gvAc_lblDados_0")).isDisplayed();
    }

}
