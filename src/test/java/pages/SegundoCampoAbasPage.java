package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import runner.RunCucumber;

public class SegundoCampoAbasPage extends RunCucumber {

    public void abrirContrato (){
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=49678337");

    }

    public void clicandoSegundoCampoAbas() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,550)", "");

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsAcionamento\']/div/ul/li[2]/a/span/span/span")).click(); // click juridico
        driver.findElement(By.id("PH_btnIncluirAcionamentoJuridico")).isDisplayed();

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsAcionamento\']/div/ul/li[3]/a/span/span/span")).click(); // click localizador
        driver.findElement(By.id("PH_btnIncluirAcionamentoLocalizador")).isDisplayed();
    }

    public void validacaoSegundoCampoAbas() {
        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsAcionamento\']/div/ul/li[4]/a/span/span/span")).click(); // click contrarias

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsAcionamento\']/div/ul/li[5]/a/span/span/span")).click(); // click boleto
    }
}
