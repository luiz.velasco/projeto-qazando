package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import runner.RunCucumber;

public class PrimeiroCampoAbasPage extends RunCucumber {

    public void consultaContrato() {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=80802945");
    }

    public void clicandoPrimeiroCampoAbas() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)", "");

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[2]/a/span/span/span")).click(); // click no endereço
        driver.findElement(By.id("PH_btnCadEndereco")).isDisplayed(); // identificando o endereço

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[3]/a/span/span/span")).click(); // Click Avalista
        driver.findElement(By.id("PH_btnCadAvalista")).isDisplayed(); // Identificando avalista

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[4]/a/span/span/span")).click();
        driver.findElement(By.id("PH_btnCadInformacao")).click();
    }

    public void validacaoPrimeiroCampoAbas() {
        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[5]/a/span/span/span")).click();
        driver.findElement(By.id("PH_btnCarregarAcompanhamentoJudicial")).isDisplayed();

        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[6]/a/span/span/span")).click();
        driver.findElement(By.id("PH_btnCarregarHistoricoEntrevista")).isDisplayed();
    }

}
