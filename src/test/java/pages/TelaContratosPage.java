package pages;

import org.openqa.selenium.By;
import runner.RunCucumber;

public class TelaContratosPage extends RunCucumber {
    LoginPage loginPage = new LoginPage();

    // Elementos
    private By selecionarVolkswagen = By.xpath("//*[@id='PH_ddlBanco']/option[41]");
    private By checkAnalitico = By.id("PH_chkAnalitico");
    private By botaoSelecionar = By.id("PH_btnSelecionar");
    private By cdContrato = By.xpath("//*[@id='PH_ddlBanco']/option[41]");

    // ações / funções / metodos
    public void loginRealizado(){
        loginPage.telaLogin();
        loginPage.preencherCredenciais();
        loginPage.fazerLogin();
    }

    public void acessarTelaFiltros() {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/RelContrato.aspx");
//        loginPage.preencherCredenciais(email, senha);
//        loginPage.fazerLogin();
    }

    public void preencherFiltros(){

        driver.findElement(By.id("PH_ddlBanco")).click();
        driver.findElement(By.xpath("//*[@id='PH_ddlBanco']/option[41]")).click();
        driver.findElement(By.id("PH_chkAnalitico")).click();
    }

    public void  clickSelecionar(){

        driver.findElement(By.id("PH_btnSelecionar")).click();
    }

    public void encontrarCdContrato()  {
        driver.findElement(By.xpath("//*[@id=\'PH_gvRel\']/tbody/tr[1]/th[1]")).isDisplayed();

    }



}
