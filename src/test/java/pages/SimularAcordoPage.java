package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import runner.RunCucumber;

public class SimularAcordoPage extends RunCucumber {

    public void acessoAoContrato() {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=9190377");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,2000)", "");

    }

    public void formaDePagamento(){
        driver.findElement(By.xpath("//*[@id=\'PH_ddlPlanoPgtoRecovery\']/option[7]")).click();
    }

    public void calcularAcordo() {
        driver.findElement(By.id("PH_gvContratoRecovery_chkParc_0")).click();
    }

    public void validacaoAcordo() {
        driver.findElement(By.id("PH_dgParcelamentoRecovery_lblcdParcela_6")).isDisplayed();
    }
}
