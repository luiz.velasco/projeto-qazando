package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.LoginPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

import java.util.concurrent.TimeUnit;

public class LoginSteps extends RunCucumber {

    LoginPage loginPage = new LoginPage();


    @Dado("^que acesso a aplicacao$")
    public void que_acesso_a_aplicacao() {
        loginPage.telaLogin();
    }

    @Dado("^acesso a tela de login$")
    public void acesso_a_tela_de_login() {

        getDriver().findElement(By.id("PH_txtCPF")).click();
    }
    @Quando("^preencho login e senha$")
    public void preencho_login_e_senha() {
        loginPage.preencherCredenciais();

    }

    @Quando("^clico em Login$")
    public void clico_em_Login() {

        loginPage.fazerLogin();

    }


    @Então("^vejo mensagem de login com sucesso$")
    public void vejo_mensagem_de_login_com_sucesso() {
        loginPage.validarAcesso();
    }

    @After
    public static void afterScenario(Scenario scenario){

        ScreenshotUtils.addScreenshotOnScenario(scenario);
    }
}




