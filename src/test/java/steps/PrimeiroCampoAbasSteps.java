package steps;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pages.PrimeiroCampoAbasPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class PrimeiroCampoAbasSteps extends RunCucumber {

    TelaContratosPage telaContratosPage = new TelaContratosPage();
    PrimeiroCampoAbasPage primeiroCampoAbasPage = new PrimeiroCampoAbasPage();

    @Dado("^acesso o crm$")
    public void acessoOCrmESenha(){
        telaContratosPage.loginRealizado();

    }

    @Quando("^faco a consulta de um contrato$")
    public void facoAConsultaDeUmContrato() {
        primeiroCampoAbasPage.consultaContrato();
       // getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/VerContrato.aspx?cdContrato=80802945");
    }

    @E("^clico nas abas telefones enderecos avalistas informacoes acompanhamento historico$")
    public void clicoNasAbasTelefonesEnderecosAvalistasInformacoesAcompanhamentoHistorico() {
        primeiroCampoAbasPage.clicandoPrimeiroCampoAbas();
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,250)", "");
//
//        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[2]/a/span/span/span")).click(); // click no endereço
//        driver.findElement(By.id("PH_btnCadEndereco")).isDisplayed(); // identificando o endereço
//
//        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[3]/a/span/span/span")).click(); // Click Avalista
//        driver.findElement(By.id("PH_btnCadAvalista")).isDisplayed(); // Identificando avalista
//
//        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[4]/a/span/span/span")).click();
//        driver.findElement(By.id("PH_btnCadInformacao")).click();
    }

    @Entao("^posso visualizar todos os campos contidos nelas$")
    public void possoVisualizarTodosOsCamposContidosNelas() {
        primeiroCampoAbasPage.validacaoPrimeiroCampoAbas();

//        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[5]/a/span/span/span")).click();
//        driver.findElement(By.id("PH_btnCarregarAcompanhamentoJudicial")).isDisplayed();
//
//        driver.findElement(By.xpath("//*[@id=\'ctl00_PH_rtsInformacoesContrato\']/div/ul/li[6]/a/span/span/span")).click();
//        driver.findElement(By.id("PH_btnCarregarHistoricoEntrevista")).isDisplayed();

    }
    @After
    public static void afterScenario(Scenario scenario){

    }
}
