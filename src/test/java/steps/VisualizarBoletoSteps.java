package steps;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pages.TelaContratosPage;
import pages.VisualizarBoletoPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class VisualizarBoletoSteps extends RunCucumber {

    TelaContratosPage telaContratosPage = new TelaContratosPage();
    VisualizarBoletoPage visualizarBoletoPage = new VisualizarBoletoPage();


    @Dado("^realizo login no crm$")
    public void realizoLoginNoCrmESenha(){

        telaContratosPage.loginRealizado();
    }

    @Quando("^consulto um contrato$")
    public void consultoUmContrato() {
        visualizarBoletoPage.consultaContrato();
    }

    @E("^clico na aba de boletos$")
    public void clicoNaAbaDeBoletos() {
        visualizarBoletoPage.cliqueBoleto();
    }

    @E("^clico em visualizar boleto$")
    public void clicoEmVisualizarBoleto() {
        visualizarBoletoPage.cliqueVisualizarBoleto();
    }

    @Entao("^posso visualizar um boleto em pdf$")
    public void possoVisualizarUmBoletoEmPdf() {
        visualizarBoletoPage.validacaoBoleto();
    }
    @After
    public static void afterScenario(Scenario scenario){


    }
}
