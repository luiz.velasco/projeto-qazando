package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import pages.LoginPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class ExportarMaillingSteps extends RunCucumber {

    TelaContratosPage telaContratosPage = new TelaContratosPage();
    @Dado("^que estou logado no bpcob$")
    public void que_estou_logado_no_bpcob_e_senha() {
    telaContratosPage.loginRealizado();

    }

    @Quando("^acesso a tela de contratos$")
    public void acesso_a_tela_de_contratos() {
        getDriver().get("https://hml99-bpcob.bellinatiperez.com.br/RelContrato.aspx");
        driver.findElement(By.id("PH_ddlBanco")).click();
        driver.findElement(By.xpath("//*[@id='PH_ddlBanco']/option[41]")).click();
        driver.findElement(By.id("PH_chkAnalitico")).click();
        driver.findElement(By.id("PH_btnSelecionar")).click();
    }

    @Quando("^aplico o filtro para exportar o mailling$")
    public void aplico_o_filtro_para_exportar_o_mailling() {
        driver.findElement(By.xpath("//*[@id=\'PH_ddlDiscador\']/option[2]")).click(); // Aspect
        driver.findElement(By.xpath("//*[@id=\'PH_ddlCampanhaDiscador\']/option[2]")).click(); // campanha

    }

    @Quando("^dou clique em download$")
    public void dou_clique_em_download(){
        driver.findElement(By.id("PH_btnExportarMailing")).click(); // clique em exportar mailling


    }

    @Entao("^e feito o download do arquivo de mailling$")
    public void e_feito_o_download_do_arquivo_de_mailling() throws InterruptedException {
        driver.findElement(By.id("PH_hplDownloadMailing")).click(); // clique em download
        Thread.sleep(2000);
        driver.findElement(By.id("PH_btnAgendarMailing")).isDisplayed();

    }
    @After
    public static void afterScenario(Scenario scenario){

    }

}