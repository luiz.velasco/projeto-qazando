package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pages.GravarAcionamentosPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class GravarAcionamento extends RunCucumber {

    TelaContratosPage telaContratosPage = new TelaContratosPage();
    GravarAcionamentosPage gravarAcionamentosPage = new GravarAcionamentosPage();

    @Dado("^que faco login no crm$")
    public void que_faco_login_no_crm_e_senha(){
        telaContratosPage.loginRealizado();
    }

    @Quando("^acesso um contrato$")
    public void acesso_um_contrato() throws InterruptedException {

        gravarAcionamentosPage.contratoParaAcionamento();
    }

    @Quando("^preencho os campos de acionamento$")
    public void preencho_os_campos_de_acionamento() throws InterruptedException {
        gravarAcionamentosPage.camposAcionamentoPreenchidos();
    }

    @Quando("^incluo o acionamento$")
    public void incluo_o_acionamento() throws InterruptedException {
        gravarAcionamentosPage.incluirAcionamento();
    }

    @Então("^o crm apresenta o acionamento gravado$")
    public void o_crm_apresenta_o_acionamento_gravado() throws InterruptedException {

        gravarAcionamentosPage.mensagemAcionamento();
    }

}
