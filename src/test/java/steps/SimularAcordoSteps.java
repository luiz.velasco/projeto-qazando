package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pages.SimularAcordoPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class SimularAcordoSteps extends RunCucumber {

    TelaContratosPage telaContratosPage = new TelaContratosPage();
    SimularAcordoPage simularAcordoPage = new SimularAcordoPage();

    @Dado("^que acesso um contrato$")
    public void que_acesso_um_contrato_e_senha(){
        telaContratosPage.loginRealizado();
        simularAcordoPage.acessoAoContrato();
    }

    @Quando("^escolho a forma de pagamento$")
    public void escolho_a_forma_de_pagamento() throws InterruptedException {
        simularAcordoPage.formaDePagamento();
    }

    @Quando("^clico em calcular$")
    public void clico_em_calcular() throws InterruptedException {
      simularAcordoPage.calcularAcordo();
    }

    @Entao("^posso visualizar a quantidade de parcelas selecionada$")
    public void posso_visualizar_a_quantidade_de_parcelas_selecionada() {

        simularAcordoPage.validacaoAcordo();
    }
    @After
    public static void afterScenario(Scenario scenario){

    }

}
