package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pages.SegundoCampoAbasPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class SegundoCampoAbasSteps extends RunCucumber {
    TelaContratosPage telaContratosPage = new TelaContratosPage();
    SegundoCampoAbasPage segundoCampoAbasPage = new SegundoCampoAbasPage();

    @Dado("^faço login no crm$")
    public void faço_login_no_crm_e_senha(){
        telaContratosPage.loginRealizado();

    }

    @Quando("^abro um contrato$")
    public void abro_um_contrbato(){
        segundoCampoAbasPage.abrirContrato();
    }

    @Quando("^clico nas abas cobranca juridico localizador contrarias e boleto$")
    public void clico_nas_abas_cobranca_juridico_localizador_contrarias_e_boleto(){
        segundoCampoAbasPage.clicandoSegundoCampoAbas();
    }

    @Entao("^posso visualizar que é possivel ver os campos em cada aba$")
    public void posso_visualizar_que_é_possivel_ver_os_campos_em_cada_aba(){
        segundoCampoAbasPage.validacaoSegundoCampoAbas();
    }
    @After
    public static void afterScenario(Scenario scenario){

    }
}
