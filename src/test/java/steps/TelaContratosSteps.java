package steps;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.*;
import org.openqa.selenium.By;
import pages.LoginPage;
import pages.TelaContratosPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class TelaContratosSteps extends RunCucumber {


    LoginPage loginPage = new LoginPage();
    TelaContratosPage telaContratosPage = new TelaContratosPage();

    @Dado("^que estou logado no crm$")
    public void que_estou_logado_no_crm_e_senha(){
        telaContratosPage.loginRealizado();

    }

    @Quando("^acesso a tela de relatorios$")
    public void acesso_a_tela_de_relatorios(){
        telaContratosPage.acessarTelaFiltros();

    }

    @Quando("^preencho os campos de filtro$")
    public void preencho_os_campos_de_filtro(){
        telaContratosPage.preencherFiltros();

    }

    @Quando("^realizo a pesquisa$")
    public void realizo_a_pesquisa(){
        telaContratosPage.clickSelecionar();

    }

    @Então("^o crm apresenta os contratos$")
    public void o_crm_apresenta_os_contratos(){
        telaContratosPage.encontrarCdContrato();


    }
    @After
    public static void afterScenario(Scenario scenario){

    }

}
