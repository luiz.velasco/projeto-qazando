package runner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class RunBase {

    public static WebDriver driver;

    // devolver meu driver que ja esta rodando
    public static WebDriver getDriver () {
        return driver;

    }

    // instanciar meu driver
    public static WebDriver getDriver (String browser) {

        if(driver != null){
            driver.quit();
        }

        switch (browser) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "chrome-ci":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                options.addArguments("--no-sandbox");
                driver = new ChromeDriver(options);
                break;
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "edge":
                throw new IllegalArgumentException("O navegador Edge ainda não esta incluido no teste");
            default:
                throw new IllegalArgumentException("Navegador não encontrado! Passe um navegador existente");

        }

        driver.manage().timeouts().implicitlyWait( 30, TimeUnit.SECONDS);

        return driver;
    }

}
